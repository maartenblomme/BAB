FROM tiangolo/uwsgi-nginx-flask:python2.7

#RUN apt update && apt install -y libmariadbclient-dev gcc  default-mysql-client

ENV NGINX_MAX_UPLOAD 16m

COPY app/requirements.txt /app/
#COPY app/basic_auth.conf /etc/nginx/conf.d/
#COPY app/htpasswd /etc/nginx/conf.d/

RUN pip install -r /app/requirements.txt

COPY ./app /app
