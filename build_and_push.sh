#!/bin/sh

set -e

if [ ! -z "$(git status --porcelain)" ]; then
  echo "repo not clean!"
  #exit
fi

git rev-parse HEAD > app/git-id
docker build -t registry.gitlab.com/maartenblomme/bab:latest .
docker push registry.gitlab.com/maartenblomme/bab:latest

rm app/git-id