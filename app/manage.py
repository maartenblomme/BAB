#!/usr/bin/env python

import os

if not os.getenv('APP_ADMIN') or\
    not os.getenv('FLASK_CONFIG') or\
    not os.getenv('SECRET_KEY'):
    raise Exception('Environment settings invalid!')

from webapp import app, db
from webapp.usermodels import User, Role, Permission
from flask.ext.script import Manager, Shell, Server
from flask.ext.migrate import Migrate, MigrateCommand

manager = Manager(app)
migrate = Migrate(app, db)


manager.add_command("runserver",
    Server(
        threaded=True,
        host='0.0.0.0'
    )
)

def make_shell_context():
    return dict(app=app, db=db, User=User, Role=Role, Permission=Permission)

manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)


@manager.command
def deploy():
    """Run deployment tasks."""
    from flask.ext.migrate import upgrade
    from app.usermodels import Role

    upgrade()

    # create user roles
    Role.insert_roles()


if __name__ == '__main__':
    manager.run()
